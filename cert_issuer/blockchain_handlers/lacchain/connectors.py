import logging

import requests

try:
    from urllib2 import urlopen, HTTPError
    from urllib import urlencode
except ImportError:
    from urllib.request import urlopen, HTTPError
    from urllib.parse import urlencode

from cert_core import Chain
from cert_issuer.models import ServiceProviderConnector
from cert_issuer.errors import BroadcastError


class LacchainServiceProviderConnector(ServiceProviderConnector):
    # param local_node indicates if a local node is running or if the tx should be broadcast to external providers
    def __init__(self, lacchain_chain, api_key, local_node=False):
        self.lacchain_chain = lacchain_chain
        self.api_key = api_key
        self.local_node = local_node

    def get_balance(self, address):
        for m in get_providers_for_chain(self.lacchain_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                balance = m.get_balance(address, self.api_key)
                return balance
            except Exception as e:
                logging.warning(e)
                pass
        return 0

    def get_address_nonce(self, address):
        for m in get_providers_for_chain(self.lacchain_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                nonce = m.get_address_nonce(address, self.api_key)
                return nonce
            except Exception as e:
                logging.warning(e)
                pass
        return 0

    def broadcast_tx(self, tx):
        for m in get_providers_for_chain(self.lacchain_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                txid = m.broadcast_tx(tx, self.api_key)
                return txid
            except Exception as e:
                logging.warning(e)
                pass
        # in case of failure:
        return '0xfail'


class EtherscanBroadcaster(object):
    def __init__(self, base_url):
        self.base_url = base_url

    def broadcast_tx(self, tx, api_token):
        tx_hex = tx

        broadcast_url = self.base_url
        response = requests.post(broadcast_url, json={
            "jsonrpc": "2.0",
            "method": "eth_sendRawTransaction",
            "params": [tx_hex],
            "id": 1
        })
        logging.error('response sendTransaction %s', response.json)
        if int(response.status_code) == 200:
            tx_id = response.json().get('result', None)
            logging.info(
                "Transaction ID obtained from broadcast through LacChain: %s", tx_id)
            return tx_id
        logging.error(
            'Error broadcasting the transaction through the LacChain API. Error msg: %s', response.text)
        raise BroadcastError(response.text)

    def get_balance(self, address, api_token):
        """
        returns the balance in wei
        with some inspiration from PyWallet
        """
        broadcast_url = self.base_url
        response = requests.post(broadcast_url, json={
            "jsonrpc": "2.0",
            "method": "eth_getBalance",
            "params": [address, 'latest'],
            "id": 53
        })
        logging.error('response balance %s', response)
        if int(response.status_code) == 200:
            balance = int(response.json().get('result', None), 0)
            logging.info('Balance check succeeded: %s', response.json())
            return balance
        raise BroadcastError(response.text)

    def get_address_nonce(self, address, api_token):
        """
        Looks up the address nonce of this address
        Neccesary for the transaction creation
        """
        broadcast_url = self.base_url
        response = requests.post(broadcast_url, json={
            "jsonrpc": "2.0",
            "method": "eth_getTransactionCount",
            "params": [address, 'latest'],
            "id": 1
        })
        logging.error('response getTransaction %s', response)
        if int(response.status_code) == 200:
            # the int(res, 0) transforms the hex nonce to int
            nonce = int(response.json().get('result', None), 0)
            logging.info('Nonce check went correct: %s', response.json())
            return nonce
        else:
            logging.info('response error checking nonce')
        raise BroadcastError(
            'Error checking the nonce through the LacChain API. Error msg: %s', response.text)



PYCOIN_BTC_PROVIDERS = "blockchain.info blockexplorer.com blockcypher.com chain.so"
PYCOIN_XTN_PROVIDERS = "blockexplorer.com"  # chain.so

# initialize connectors
connectors = {}

# Configure lacchain mainnet connectors
eth_provider_list = []
eth_provider_list.append(EtherscanBroadcaster('http://35.239.55.127/nodo'))
connectors[Chain.lacchain_mainnet] = eth_provider_list

# Configure lacchain testnet testnet connectors
rop_provider_list = []
rop_provider_list.append(EtherscanBroadcaster(
    'http://34.123.38.34/nodo'))
connectors[Chain.lacchain_testnet] = rop_provider_list


def get_providers_for_chain(chain, local_node=False):
    return connectors[chain]
