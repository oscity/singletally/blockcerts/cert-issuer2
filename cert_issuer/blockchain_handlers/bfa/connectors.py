
from cert_issuer.errors import BroadcastError
from cert_issuer.models import ServiceProviderConnector
from cert_core import Chain
import logging

import requests
import sys
sys.path.append('/home/cert-issuer')
sys.path.append('/home/cert-core')

try:
    from urllib2 import urlopen, HTTPError
    from urllib import urlencode
except ImportError:
    from urllib.request import urlopen, HTTPError
    from urllib.parse import urlencode


class BfaServiceProviderConnector(ServiceProviderConnector):
    # param local_node indicates if a local node is running or if the tx should be broadcast to external providers
    def __init__(self, bfa_chain, api_key, local_node=False):
        self.bfa_chain = bfa_chain
        self.api_key = api_key
        self.local_node = local_node

    def get_balance(self, address):
        for m in get_providers_for_chain(self.bfa_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                balance = m.get_balance(address, self.api_key)
                return balance
            except Exception as e:
                logging.warning(e)
                pass
        return 0

    def get_address_nonce(self, address):
        for m in get_providers_for_chain(self.bfa_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                nonce = m.get_address_nonce(address, self.api_key)
                return nonce
            except Exception as e:
                logging.warning(e)
                pass
        return 0

    def broadcast_tx(self, tx):
        for m in get_providers_for_chain(self.bfa_chain, self.local_node):
            try:
                logging.debug('m=%s', m)
                txid = m.broadcast_tx(tx, self.api_key)
                return txid
            except Exception as e:
                logging.warning(e)
                pass
        # in case of failure:
        return '0xfail'


class EtherscanBroadcaster(object):
    def __init__(self, base_url):
        self.base_url = base_url

    def broadcast_tx(self, tx, api_token):
        tx_hex = tx

        broadcast_url = self.base_url
        # if api_token:
        #     broadcast_url += '&apikey=%s' % api_token
        response = requests.post(broadcast_url, json={
            "method": "sendRawTransaction",
            "tx": tx_hex,
        })
        logging.error('response sendTransaction %s', response.json)
        if int(response.status_code) == 200:
            tx_id = response.json().get('result', None)
            logging.info(
                "Transaction ID obtained from broadcast through BFA: %s", tx_id)
            return tx_id
        logging.error(
            'Error broadcasting the transaction through the BFA API. Error msg: %s', response.text)
        raise BroadcastError(response.text)

    def get_balance(self, address, api_token):
        """
        returns the balance in wei
        with some inspiration from PyWallet
        """
        broadcast_url = self.base_url
        # if api_token:
        #     broadcast_url += '&apikey=%s' % api_token
        response = requests.get(broadcast_url, json={
            "method": "getBalance",
            "address": address,
        })
        logging.error('response balance %s', response)
        if int(response.status_code) == 200:
            balance = int(response.json().get('result', None))
            logging.info('Balance check succeeded: %s', response.json())
            return balance
        raise BroadcastError(response.text)

    def get_address_nonce(self, address, api_token):
        """
        Looks up the address nonce of this address
        Neccesary for the transaction creation
        """
        broadcast_url = self.base_url
        # if api_token:
        #     broadcast_url += '&apikey=%s' % api_token
        response = requests.get(broadcast_url, json={
            "method": "getTransactionCount",
            "address": address
        })
        logging.error('response getTransaction %s', response)
        if int(response.status_code) == 200:
            # the int(res, 0) transforms the hex nonce to int
            nonce = int(response.json().get('result', None))
            logging.info('Nonce check went correct: %s', response.json())
            return nonce
        else:
            logging.info('response error checking nonce')
        raise BroadcastError(
            'Error checking the nonce through the BFA API. Error msg: %s', response.text)


PYCOIN_BTC_PROVIDERS = "blockchain.info blockexplorer.com blockcypher.com chain.so"
PYCOIN_XTN_PROVIDERS = "blockexplorer.com"  # chain.so

# initialize connectors
connectors = {}

# Configure Ethereum mainnet connectors
eth_provider_list = []
eth_provider_list.append(EtherscanBroadcaster('http://35.232.162.22/api/v1/'))
connectors[Chain.bfa_mainnet] = eth_provider_list

# Configure Ethereum Ropsten testnet connectors
rop_provider_list = []
rop_provider_list.append(EtherscanBroadcaster('http://104.198.40.138/api/v1/'))
connectors[Chain.bfa_testnet] = rop_provider_list


def get_providers_for_chain(chain, local_node=False):
    return connectors[chain]
